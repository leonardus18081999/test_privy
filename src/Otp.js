import './Otp.css';
// import * as React from 'react';
import Moment from 'moment';
import OTPInput, { ResendOTP } from 'otp-input-react';
import React, { useState } from "react";

export default function Otp() {

  const [OTP, setOTP] = useState("");

  const current = new Date().toLocaleDateString();
  const date = Moment({current}).format("MMM DD, YYYY");

  return (
    <div>
      <div className="Card">
        <div><b>Today {date}{" "}</b></div>
        <div className="tab">
        <div className="title"><b>OTP Verification</b></div>
        <div className="subTitle"><small>Insert OTP code sent to your phone</small></div>
        <form className="box">
        <div className="otp">
        <OTPInput
          value={OTP}
          onChange={setOTP}
          autoFocus
          OTPLength={4}
          otpType="number"
          disabled={false}
          secure
        />
        </div>
          <div className="field">
            <button type="submit" value="Submit" className='Otp'>Verify</button>
          </div>
        </form>
      </div>
      </div>
      <div className="resendOtp">
      <ResendOTP
          renderTime={() => React.Fragment}
          renderButton={(buttonProps) => {
            return (
              <a href='/' {...buttonProps}>
                {buttonProps.remainingTime !== 0 ? `Please wait for ${buttonProps.remainingTime} sec` : "Resend"}
              </a>
            );
          }}
        />
        </div>
    </div>
  );
}