import './Login.css';
import * as React from 'react';
import InputLabel from '@mui/material/InputLabel';
import FormControl from '@mui/material/FormControl';
import NativeSelect from '@mui/material/NativeSelect';
import Box from '@mui/material/Box';
import Tab from '@mui/material/Tab';
import TabContext from '@mui/lab/TabContext';
import TabList from '@mui/lab/TabList';
import TabPanel from '@mui/lab/TabPanel';
import Moment from 'moment';

export default function Login() {

  const current = new Date().toLocaleDateString();
  const date = Moment({current}).format("MMM DD YY");

  const [value, setValue] = React.useState('1');

  const handleChange = (event, newValue) => {
    setValue(newValue);
  };

  return (
    <div>
      <div className="Card">
        <div><b>Today {date}{" "}</b></div>
        <div className="tab">
        <TabContext value={value}>
        <Box sx={{ borderBottom: 1, borderColor: 'divider' }}>
          <TabList onChange={handleChange} aria-label="lab API tabs example">
            <Tab label="Login" value="1" />
            <Tab label="Registration" value="2" />
          </TabList>
        </Box>
        <TabPanel value="1">
        <div><b>Login Account</b></div>
        <form className="box">
          <div className="field">
            <label className="label">Phone Number</label>
            <div>
              <input type="text" className="input" placeholder="Phone Number"/>
            </div>
          </div>
          <div className="field">
            <label className="label">Password</label>
            <div>
              <input type="password" className="input" placeholder="********"/>
            </div>
          </div>
          <div className="field">
            <button type="submit" value="Submit" className='Login'>Login</button>
          </div>
          <div className="field">
            <button className='Reset'>Reset</button>
          </div>
        </form>
        </TabPanel>
        <TabPanel value="2">
        <div><b>Create New Account</b></div>
        <div><small>Before you can join here, please create new account</small></div>
        <div className="subTitle"><b>Account Detail</b></div>
        <form className="box">
          <div className="field-select">
          <FormControl fullWidth>
            <InputLabel variant="standard" htmlFor="uncontrolled-native">
              Select Country
            </InputLabel>
            <NativeSelect
              defaultValue={62}
              inputProps={{
              name: 'Indonesia (+62)',
              id: 'uncontrolled-native',
              }}
              >
              <option value={62}>Indonesia (+62)</option>
            </NativeSelect>
          </FormControl>
          </div>
          <div className="field">
            <label className="label">Phone Number</label>
            <div>
              <input type="text" className="input" placeholder="Phone Number"/>
            </div>
          </div>
          <div className="field">
            <label className="label">Password</label>
            <div>
              <input type="password" className="input" placeholder="********"/>
            </div>
          </div>
          <div className="field">
            <button type="submit" value="Submit" className='Login'>Login</button>
          </div>
          <div className="field">
            <button className='Reset'>Reset</button>
          </div>
        </form>
        </TabPanel>
      </TabContext>
      </div>
      </div>
    </div>
  );
}