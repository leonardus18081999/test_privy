import privy from './privy.svg';
import laptop from './laptop.jpg';
import Login from './Login';
import './App.css';

function App() {
  return (
    <div className="Row">
      <div>
        <img src={privy} className="Privy-logo" alt="privy" />
        <h2 className="Privy-title">Welcome to Prisign</h2>
        <p className="Privy-desc">Is a personal data platform, you canupdate your information about yourself, customize your profile and change a lot of things.</p>
        <img src={laptop} className="Privy-desc" alt="laptop" height="300"/>
      </div>
        <Login />
    </div>
  );
}

export default App;
